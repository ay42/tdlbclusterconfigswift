import SwiftUI
import LBSwiftFrameworkiOS

struct ContentView: View {
    @State private var selection = 0
 
    var body: some View {
        TabView(selection: $selection) {
            PageView<FirstRenderer, FirstControlView>()
                .tabItem {
                    VStack {
                        Image(systemName: "square.grid.4x3.fill")
                        Text("Cluster")
                    }
                }
                .tag(0)
            PageView<SecondRenderer, SecondControlView>()
                .tabItem {
                    VStack {
                        Image(systemName: "square.grid.4x3.fill")
                        Text("Node")
                    }
                }
                .tag(1)
        }
        .accentColor(.blue)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
