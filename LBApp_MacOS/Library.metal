//
//  Library.metal
//  ClusterConfigDesktop
//
//  Created by  Nile Ó Broin on 13/04/2019.
//  Copyright © 2019  Nile Ó Broin. All rights reserved.
//

#include <metal_stdlib>

#include "../LBSwiftCode/Rendering/Common.h"

using namespace metal;

constant float3 kLightDirection(-0.43, -0.8, -0.43);

struct ProjectedVertex
{
    float4 position [[position]];
    float3 normal [[user(normal)]];
};

vertex ProjectedVertex vertex_shader(constant Vertex *vertices [[buffer(0)]],
                                     constant Uniforms &uniforms [[buffer(1)]],
                                     constant PerInstanceUniforms *perInstanceUniforms [[buffer(2)]],
                                     ushort vid [[vertex_id]],
                                     ushort iid [[instance_id]])
{
    float4x4 instanceModelMatrix = perInstanceUniforms[iid].modelMatrix;
    float3x3 instanceNormalMatrix = perInstanceUniforms[iid].normalMatrix;
    
    ProjectedVertex outVert;
    outVert.position = uniforms.viewProjectionMatrix * instanceModelMatrix * float4(vertices[vid].position);
    outVert.normal = instanceNormalMatrix * float4(vertices[vid].normal).xyz;
    
    return outVert;
}

fragment half4 fragment_shader(ProjectedVertex vert [[stage_in]])
{
    float diffuseIntensity = max(0.33, dot(normalize(vert.normal), -kLightDirection));
    float4 diffuseColor(0.5, 0.5, 0.5, 1.0);
    float4 color = diffuseColor * diffuseIntensity;
    return half4(color.r, color.g, color.b, 1);
}

