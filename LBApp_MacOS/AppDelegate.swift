//
//  AppDelegate.swift
//  TD_Cluster_Config_MacOS
//
//  Created by  Nile Ó Broin on 15/04/2019.
//  Copyright © 2019  Nile Ó Broin. All rights reserved.
//

import Cocoa
import SwiftUI
import LBSwiftFrameworkMacOS

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var window: NSWindow!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        window = NSWindow(
                    contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
                    styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
                    backing: .buffered, defer: false)
                window.center()
                window.setFrameAutosaveName("Main Window")
        
        let contentView = ContentView().environmentObject(GridStore())
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)

//        setupDisplayLink()

//        contentView.rotationCallback = { [weak self] vector in
//            self?.renderer?.rotationVector = vector
//        }
//
//        contentView.controlView.updateCallback = { [weak self] grid in
//            self?.renderer?.setGrid(grid)
//        }
//
//        contentView.controlView.loadCallback = { [weak self] in
//            let panel = NSOpenPanel()
//            panel.begin { response in
//                if response == .OK {
//                    guard let url = panel.urls.first else { return }
//                    self?.loadGrid(url)
//                }
//            }
//        }
//
//        contentView.controlView.saveCallback = { [weak self] in
//            guard let window = self?.window else { return }
//
//            let panel = NSSavePanel()
//            panel.representedFilename = "grid.json"
//            panel.beginSheetModal(for: window) { response in
//                if response == .OK {
//                    guard let url = panel.url else { return }
//                    self?.saveGrid(url)
//                }
//            }
//        }

//        if let grid = renderer?.getGrid() {
//            contentView.controlView.setGrid(grid)
//        }

    }
//
//    func applicationWillTerminate(_ aNotification: Notification) {
//        if let link = contentView.displayLink {
//            CVDisplayLinkStop(link)
//        }
//    }

//    private func setupDisplayLink() {
//        CVDisplayLinkCreateWithActiveCGDisplays(&contentView.displayLink)
//
//        if let link = contentView.displayLink {
//            CVDisplayLinkSetOutputHandler(link) { [weak self] _, _, _, _, _ in
//                self?.contentView.renderer?.draw()
//                return kCVReturnSuccess
//            }
//
//            CVDisplayLinkStart(link)
//        }
//    }

//    private func loadGrid(_ url: URL) {
//        do {
//            let data = try Data(contentsOf: url)
//            let decoder = JSONDecoder()
//            let grid = try decoder.decode(Grid.self, from: data)
//            contentView.renderer?.setGrid(grid)
//        } catch {
//            print("Error: can't load grid: \(error)")
//        }
//    }
//
//    private func saveGrid(_ url: URL) {
//        guard let grid = contentView.renderer?.getGrid() else { return }
//
//        DispatchQueue.global().async {
//            do {
//                let encoder = JSONEncoder()
//                let data = try encoder.encode(grid)
//                try data.write(to: url)
//            } catch {
//                print("Error: can't save grid: \(error)")
//            }
//        }
//    }
}
