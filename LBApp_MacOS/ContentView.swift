import SwiftUI
import LBSwiftFrameworkMacOS

struct ContentView: View {
    @State private var selection = 0
    
    var body: some View {
        TabView(selection: $selection) {
            PageView<FirstRenderer, FirstControlView>()
                .tabItem {
                    Text("Cluster")
                }
            .tag(0)
            PageView<SecondRenderer, SecondControlView>()
                .tabItem {
                    Text("Node")
                }
                .tag(1)
        }
        .frame(minWidth: 300, maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
