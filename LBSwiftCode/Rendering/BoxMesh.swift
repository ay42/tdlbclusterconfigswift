//
//  BoxMesh.swift
//  TD Cluster Config
//
//  Created by  Nile ÓBroin on 13/04/2019.
//  Copyright © 2019  Nile ÓBroin. All rights reserved.
//

import MetalKit

class BoxMesh {
    
    let vertexBuffer: MTLBuffer
    let indexBuffer: MTLBuffer
    
    var indexCount: Int {
        return indexArray.count
    }
    
    private let vertexArray: [Vertex] = [
        // Front
        Vertex(position: vector_float4(-1.0, 1.0, 1.0, 1.0), normal: vector_float4(0.0, 0.0, 1.0, 1.0)),
        Vertex(position: vector_float4(-1.0, -1.0, 1.0, 1.0), normal: vector_float4(0.0, 0.0, 1.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, 1.0, 1.0), normal: vector_float4(0.0, 0.0, 1.0, 1.0)),
        Vertex(position: vector_float4(1.0, 1.0, 1.0, 1.0), normal: vector_float4(0.0, 0.0, 1.0, 1.0)),
        
        // Left
        Vertex(position: vector_float4(-1.0, 1.0, -1.0, 1.0), normal: vector_float4(-1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(-1.0, -1.0, -1.0, 1.0), normal: vector_float4(-1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(-1.0, -1.0, 1.0, 1.0), normal: vector_float4(-1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(-1.0, 1.0, 1.0, 1.0), normal: vector_float4(-1.0, 0.0, 0.0, 1.0)),
        
        // Right
        Vertex(position: vector_float4(1.0, 1.0, 1.0, 1.0), normal: vector_float4(1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, 1.0, 1.0), normal: vector_float4(1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, -1.0, 1.0), normal: vector_float4(1.0, 0.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, 1.0, -1.0, 1.0), normal: vector_float4(1.0, 0.0, 0.0, 1.0)),
        
        // Top
        Vertex(position: vector_float4(-1.0, 1.0, -1.0, 1.0), normal: vector_float4(0.0, 1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(-1.0, 1.0, 1.0, 1.0), normal: vector_float4(0.0, 1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, 1.0, 1.0, 1.0), normal: vector_float4(0.0, 1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, 1.0, -1.0, 1.0), normal: vector_float4(0.0, 1.0, 0.0, 1.0)),
        
        // Bottom
        Vertex(position: vector_float4(-1.0, -1.0, 1.0, 1.0), normal: vector_float4(0.0, -1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(-1.0, -1.0, -1.0, 1.0), normal: vector_float4(0.0, -1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, -1.0, 1.0), normal: vector_float4(0.0, -1.0, 0.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, 1.0, 1.0), normal: vector_float4(0.0, -1.0, 0.0, 1.0)),
        
        // Back
        Vertex(position: vector_float4(1.0, 1.0, -1.0, 1.0), normal: vector_float4(0.0, 0.0, -1.0, 1.0)),
        Vertex(position: vector_float4(1.0, -1.0, -1.0, 1.0), normal: vector_float4(0.0, 0.0, -1.0, 1.0)),
        Vertex(position: vector_float4(-1.0, -1.0, -1.0, 1.0), normal: vector_float4(0.0, 0.0, -1.0, 1.0)),
        Vertex(position: vector_float4(-1.0, 1.0, -1.0, 1.0), normal: vector_float4(0.0, 0.0, -1.0, 1.0))
    ]
    
    private let indexArray: [UInt16] = [
        0, 1, 2,
        0, 2, 3,
        4, 5, 6,
        4, 6, 7,
        8, 9, 10,
        8, 10, 11,
        12, 13, 14,
        12, 14, 15,
        16, 17, 18,
        16, 18, 19,
        20, 21, 22,
        20, 22, 23
    ]
    
    init(_ device: MTLDevice) {
        vertexBuffer = makeVertexBuffer(device: device, array: vertexArray)
        indexBuffer = makeIndexBuffer(device: device, array: indexArray)
    }
}

private func makeVertexBuffer(device: MTLDevice, array: [Vertex]) -> MTLBuffer {
    let data = Data(bytes: array, count: MemoryLayout<Vertex>.stride * array.count)
    return data.withUnsafeBytes { (p: UnsafePointer<UInt8>) -> MTLBuffer in
        if let buffer = device.makeBuffer(bytes: p, length: data.count, options: .storageModeShared) {
            return buffer
        }
        
        fatalError()
    }
}

private func makeIndexBuffer(device: MTLDevice, array: [UInt16]) -> MTLBuffer {
    let data = Data(bytes: array, count: MemoryLayout<UInt16>.stride * array.count)
    return data.withUnsafeBytes { (p: UnsafePointer<UInt8>) -> MTLBuffer in
        if let buffer = device.makeBuffer(bytes: p, length: data.count, options: .storageModeShared) {
            return buffer
        }
        
        fatalError()
    }
}
