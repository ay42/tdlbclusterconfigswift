import Foundation
import MetalKit

public protocol Renderer {
    associatedtype RenderedGrid
    init(layer: CAMetalLayer)
    var rotationVector: vector_float2 { get set }
    var grid: RenderedGrid { get set }
    func draw()
    
}
