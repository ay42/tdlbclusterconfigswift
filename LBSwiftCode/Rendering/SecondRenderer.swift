//
//  SecondRenderer.swift
//  LBSwiftFrameworkiOS
//
//  Created by MacMaster on 7/31/19.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//

import Foundation

public final class SecondRenderer: Renderer {
    
    public var rotationVector = vector_float2()
    
    private let layer: CAMetalLayer
    
    // Long-lived Metal objects
    private var commandQueue: MTLCommandQueue?
    private var renderPipeline: MTLRenderPipelineState?
    private var depthState: MTLDepthStencilState?
    private var depthTexture: MTLTexture?
    
    // Resources
    private var mesh: BoxMesh?
    private var sharedUniformBuffer: MTLBuffer?
    private var boxUniformBuffer: MTLBuffer?
    private var boxSizeBuffer: MTLBuffer?
    
    // Parameters
    private let origi_scale_x : Float = 3.6
    private var scale_main : vector_float3 = vector_float3(3.6, 3.6, 3.6)
    private var scale_secondary : vector_float3 = vector_float3(0.4, 0.4, 0.4)
    private var spacing : vector_float3 = vector_float3(2, 2, 2)
    
    private var zoom : Int32 = 1
    
    private var cameraPosition = vector_float3(0, -5, 30)
    private var xAngle = Float(0)
    private var yAngle = Float(0)
    
    public var grid = SecondaryGrid(depth: vector_float3(0.4, 0.4, 0.4), spacing: 2) {
        didSet {
            scale_secondary = grid.depth
            spacing = vector_float3(repeating: Float(grid.nDevice))
            zoom = grid.scale
            needsUpdate = true
        }
    }
    
    private var needsUpdate = true
    
    private var boxes = [Box]()
    
    public init(layer: CAMetalLayer) {
        self.layer = layer
        
        setupMetal()
        
        do {
            try createPipeline()
        } catch {
            print("Renderer: can't create pipeline: \(error)")
        }
        
        buildBoxes()
    }
    
    public func draw() {
        updateUniforms()
        
        guard let drawable = layer.nextDrawable() else { return }
        
        if depthTexture?.width != Int(layer.drawableSize.width) ||
            depthTexture?.height != Int(layer.drawableSize.height) {
            buildDepthTexture()
        }
        
        let renderPass = createRenderPassWithColorAttachmentTexture(drawable.texture)
        
        guard let commandBuffer = commandQueue?.makeCommandBuffer(),
            let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPass),
            let pipelineState = renderPipeline else { return }
        
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setDepthStencilState(depthState)
        commandEncoder.setFrontFacing(.counterClockwise)
        commandEncoder.setCullMode(.back)
        
        drawBoxesWithCommandEncoder(commandEncoder)
        
        commandEncoder.endEncoding()
        
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
    
    private func setupMetal() {
        layer.device = MTLCreateSystemDefaultDevice()
        layer.pixelFormat = .bgra8Unorm
    }
    
    private func createPipeline() throws {
        guard let device = layer.device else { return }
        
        commandQueue = device.makeCommandQueue()
        
        let vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].format = .float4
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0
        
        vertexDescriptor.attributes[1].format = .float4
        vertexDescriptor.attributes[1].offset = MemoryLayout<vector_float4>.stride
        vertexDescriptor.attributes[1].bufferIndex = 0
        
        vertexDescriptor.layouts[0].stepFunction = .perVertex
        vertexDescriptor.layouts[0].stride = MemoryLayout<Vertex>.stride
        
        guard let library = device.makeDefaultLibrary() else {
            return
        }
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = library.makeFunction(name: "vertex_shader")
        pipelineDescriptor.fragmentFunction = library.makeFunction(name: "fragment_shader")
        pipelineDescriptor.vertexDescriptor = vertexDescriptor
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float
        
        renderPipeline = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        
        let depthDescriptor = MTLDepthStencilDescriptor()
        depthDescriptor.isDepthWriteEnabled = true
        depthDescriptor.depthCompareFunction = .less
        
        depthState = device.makeDepthStencilState(descriptor: depthDescriptor)
        
        sharedUniformBuffer = device.makeBuffer(length: MemoryLayout<Uniforms>.size, options: .storageModeShared)
    }
    
    private func buildDepthTexture() {
        guard let device = layer.device else { return }
        
        let size = self.layer.drawableSize
        
        let descriptor = MTLTextureDescriptor.texture2DDescriptor(
            pixelFormat: .depth32Float,
            width: Int(size.width),
            height: Int(size.height),
            mipmapped: false
        )
        
        descriptor.usage = .renderTarget
        
        //Added for MacOS app
        descriptor.storageMode = .private
        
        depthTexture = device.makeTexture(descriptor: descriptor)
    }
    
    private func buildBoxes() {
        guard let device = layer.device else { return }
        
        if mesh == nil {
            mesh = BoxMesh(device)
        }
        
        boxes.removeAll()
        
        scale_secondary = grid.depth
        spacing = vector_float3(repeating: Float(grid.nDevice) / 6 + 1.0)
        
        let outer_X = scale_main.x + spacing.x + scale_secondary.x
        let outer_Y = scale_main.y + spacing.y + scale_secondary.y
        let outer_Z = scale_main.z + spacing.z + scale_secondary.z
        
        // Boxes Center
        scale_main.x = origi_scale_x + 0.1 * Float(grid.nDevice - 1)
        
        if grid.nDevice == 1 {
            boxes.append(Box(position: vector_float3(0, 0, 0), size: vector_float3(scale_main.x, scale_main.y, scale_main.z)))
        } else {
            for i in 0..<grid.nDevice {
                let x = scale_main.x * 2.0 / Float(grid.nDevice) * (Float(i) + 0.5) - scale_main.x
                let width_x = scale_main.x / Float(grid.nDevice) - 0.1
                
                boxes.append(Box(position: vector_float3(x, 0, 0), size: vector_float3(width_x, scale_main.y, scale_main.z)))
            }
        }
        
        boxes.append(Box(position: vector_float3(-outer_X, -outer_Y, 0), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_main.z)))
        boxes.append(Box(position: vector_float3(0, -outer_Y, 0), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_main.z)))
        boxes.append(Box(position: vector_float3(outer_X, -outer_Y, 0), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_main.z)))
        
        boxes.append(Box(position: vector_float3(-outer_X, 0, 0), size: vector_float3(scale_secondary.x, scale_main.y, scale_main.z)))
        boxes.append(Box(position: vector_float3(-outer_X, outer_Y, 0), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_main.z)))
        
        boxes.append(Box(position: vector_float3(outer_X, outer_Y, 0), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_main.z)))
        
        boxes.append(Box(position: vector_float3(outer_X, 0, 0), size: vector_float3(scale_secondary.x, scale_main.y, scale_main.z)))
        boxes.append(Box(position: vector_float3(0, outer_Y, 0), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_main.z)))
        
        // Box front
        boxes.append(Box(position: vector_float3(-outer_X, -outer_Y, outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(0, -outer_Y, outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(outer_X, -outer_Y, outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(-outer_X, 0, outer_Z), size: vector_float3(scale_secondary.x, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(-outer_X, outer_Y, outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(0, 0, outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(outer_X, outer_Y, outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(outer_X, 0, outer_Z), size: vector_float3(scale_secondary.x, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(0, outer_Y, outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_secondary.z)))
        
        // Box back
        boxes.append(Box(position: vector_float3(-outer_X, -outer_Y, -outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(0, -outer_Y, -outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(outer_X, -outer_Y, -outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(-outer_X, 0, -outer_Z), size: vector_float3(scale_secondary.x, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(-outer_X, outer_Y, -outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(0, 0, -outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(outer_X, outer_Y, -outer_Z), size: vector_float3(scale_secondary.x, scale_secondary.y, scale_secondary.z)))
        
        boxes.append(Box(position: vector_float3(outer_X, 0, -outer_Z), size: vector_float3(scale_secondary.x, scale_main.y, scale_secondary.z)))
        boxes.append(Box(position: vector_float3(0, outer_Y, -outer_Z), size: vector_float3(origi_scale_x/*scale_main.x*/, scale_secondary.y, scale_secondary.z)))
        
        let length = MemoryLayout<PerInstanceUniforms>.size * boxes.count
        boxUniformBuffer = device.makeBuffer(length: length, options: .storageModeShared)
        
        let length1 = MemoryLayout<BoxSizeUniforms>.size * boxes.count
        boxSizeBuffer = device.makeBuffer(length: length1, options: .storageModeShared)
    }
    
    private func updateUniforms() {
        updateBoxes()
        updateSharedUniforms()
    }
    
    private func updateBoxes() {
        if needsUpdate {
            needsUpdate = false
            buildBoxes()
        }
        
        if rotationVector.x != 0 {
            xAngle += (rotationVector.x > 0 ? 3 : -3) * Float.pi / 180
        }
        
        if rotationVector.y != 0 {
            yAngle += (rotationVector.y > 0 ? 3 : -3) * Float.pi / 180
        }
        
        var rotationMatrix = matrix_identity()
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_rotation(axis: vector_float3(0, 1, 0), angle: xAngle))
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_rotation(axis: vector_float3(1, 0, 0), angle: yAngle))
        
        let scale = 1 - 0.1 * Float(grid.nDevice) * 8.0 / 64.0
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_uniform_scale(scale))
        
        var offset = 0
        boxes.forEach { box in
            let t = matrix_multiply(matrix_translation(box.position), matrix_uniform_scale(Float(1/*grid.scale*/)))
            let modelMatrix = matrix_multiply(rotationMatrix, t)
            
            let capacity = MemoryLayout<PerInstanceUniforms>.size
            if let p = boxUniformBuffer?.contents().advanced(by: offset).bindMemory(to: PerInstanceUniforms.self, capacity: capacity) {
                p.pointee.modelMatrix = modelMatrix
                p.pointee.normalMatrix = matrix_upper_left3x3(modelMatrix)
                p.pointee.size = vector_float4(box.size.x, box.size.y, box.size.z, 1.0)
            }
            
            offset += MemoryLayout<PerInstanceUniforms>.stride
        }
        
    }
    
    private func updateSharedUniforms() {
        let viewMatrix = matrix_translation(-cameraPosition)
        
        let aspect = Float(layer.drawableSize.width / layer.drawableSize.height)
        let fov = (aspect > 1) ? (Float.pi / 4) : (Float.pi / 3)
        let projectionMatrix = matrix_perspective_projection(aspect: aspect, fovy: fov, near: 0.1, far: 100)
        
        if let p = sharedUniformBuffer?.contents().bindMemory(to: Uniforms.self, capacity: MemoryLayout<Uniforms>.size) {
            p.pointee.viewProjectionMatrix = matrix_multiply(projectionMatrix, viewMatrix)
        }
    }
    
    private func createRenderPassWithColorAttachmentTexture(_ texture: MTLTexture) -> MTLRenderPassDescriptor {
        let renderPass = MTLRenderPassDescriptor()
        renderPass.colorAttachments[0].texture = texture
        renderPass.colorAttachments[0].loadAction = .clear
        renderPass.colorAttachments[0].storeAction = .store
        renderPass.colorAttachments[0].clearColor = MTLClearColorMake(1.0, 1.0, 1.0, 1.0)
        
        renderPass.depthAttachment.texture = depthTexture
        renderPass.depthAttachment.loadAction = .clear
        renderPass.depthAttachment.storeAction = .store
        renderPass.depthAttachment.clearDepth = 1.0
        
        return renderPass
    }
    
    func drawBoxesWithCommandEncoder(_ commandEncoder: MTLRenderCommandEncoder) {
        guard let vertexBuffer = mesh?.vertexBuffer,
            let indexBuffer = mesh?.indexBuffer,
            let indexCount = mesh?.indexCount else { return }
        
        commandEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        commandEncoder.setVertexBuffer(sharedUniformBuffer, offset: 0, index: 1)
        commandEncoder.setVertexBuffer(boxUniformBuffer, offset: 0, index: 2)
        commandEncoder.setVertexBuffer(boxSizeBuffer, offset: 0, index: 3)
        
        commandEncoder.drawIndexedPrimitives(
            type: .triangle,
            indexCount: indexCount,
            indexType: .uint16,
            indexBuffer: indexBuffer,
            indexBufferOffset: 0,
            instanceCount: boxes.count
        )
    }
}
