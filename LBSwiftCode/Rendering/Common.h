//
//  Common.h
//  TD Cluster Config
//
//  Created by  Nile ÓBroin on 13/04/2019.
//  Copyright © 2019  Nile ÓBroin. All rights reserved.
//

#ifndef Common_h
#define Common_h

#import <simd/simd.h>

typedef struct
{
    vector_float4 position;
    vector_float4 normal;
} Vertex;

typedef struct
{
    matrix_float4x4 viewProjectionMatrix;
} Uniforms;

typedef struct
{
    matrix_float4x4 modelMatrix;
    matrix_float3x3 normalMatrix;
    vector_float4 size;
} PerInstanceUniforms;

typedef struct
{
    vector_float4 size;
} BoxSizeUniforms;

#endif /* Common_h */
