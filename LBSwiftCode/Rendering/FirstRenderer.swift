//
//  Renderer.swift
//  TD Cluster Config
//
//  Created by  Nile ÓBroin on 13/04/2019.
//  Copyright © 2019  Nile ÓBroin. All rights reserved.
//

import MetalKit
import Combine

struct Box {
    var position: vector_float3
    var size: vector_float3
}

public final class FirstRenderer: Renderer {    
    public var rotationVector = vector_float2()
    
    private let layer: CAMetalLayer
    
    // Long-lived Metal objects
    private var commandQueue: MTLCommandQueue?
    private var renderPipeline: MTLRenderPipelineState?
    private var depthState: MTLDepthStencilState?
    private var depthTexture: MTLTexture?
    
    // Resources
    private var mesh: BoxMesh?
    private var sharedUniformBuffer: MTLBuffer?
    private var boxUniformBuffer: MTLBuffer?
    private var boxSizeBuffer: MTLBuffer?
    
    // Parameters
    private var cameraPosition = vector_float3(0, -5, 30)//(0, 2, 30)
    private var xAngle = Float(0)
    private var yAngle = Float(0)
    
    public var grid = Grid(count: vector_int3(4, 4, 4), scale: 1) {
        didSet {
            needsUpdate = true
        }
    }
    
    private var needsUpdate = true
    
    private var boxes = [Box]()
    
    
    
    public init(layer: CAMetalLayer) {
        self.layer = layer
        setupMetal()
        
        do {
            try createPipeline()
        } catch {
            print("Renderer: can't create pipeline: \(error)")
        }
        
        buildBoxes()
    }
    
    public func draw() {
        updateUniforms()
        
        guard let drawable = layer.nextDrawable() else { return }
        
        if depthTexture?.width != Int(layer.drawableSize.width) ||
            depthTexture?.height != Int(layer.drawableSize.height) {
            buildDepthTexture()
        }
        
        let renderPass = createRenderPassWithColorAttachmentTexture(drawable.texture)
        
        guard let commandBuffer = commandQueue?.makeCommandBuffer(),
            let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPass),
            let pipelineState = renderPipeline else { return }
        
        commandEncoder.setRenderPipelineState(pipelineState)
        commandEncoder.setDepthStencilState(depthState)
        commandEncoder.setFrontFacing(.counterClockwise)
        commandEncoder.setCullMode(.back)
        
        drawBoxesWithCommandEncoder(commandEncoder)
        
        commandEncoder.endEncoding()
        
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
    
    private func setupMetal() {
        layer.device = MTLCreateSystemDefaultDevice()
        layer.pixelFormat = .bgra8Unorm
    }
    
    private func createPipeline() throws {
        guard let device = layer.device else { return }
        
        commandQueue = device.makeCommandQueue()
        
        let vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].format = .float4
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0
        
        vertexDescriptor.attributes[1].format = .float4
        vertexDescriptor.attributes[1].offset = MemoryLayout<vector_float4>.stride
        vertexDescriptor.attributes[1].bufferIndex = 0
        
        vertexDescriptor.layouts[0].stepFunction = .perVertex
        vertexDescriptor.layouts[0].stride = MemoryLayout<Vertex>.stride
        
        guard let library = device.makeDefaultLibrary() else {
            return
        }
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = library.makeFunction(name: "vertex_shader")
        pipelineDescriptor.fragmentFunction = library.makeFunction(name: "fragment_shader")
        pipelineDescriptor.vertexDescriptor = vertexDescriptor
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float
        
        renderPipeline = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        
        let depthDescriptor = MTLDepthStencilDescriptor()
        depthDescriptor.isDepthWriteEnabled = true
        depthDescriptor.depthCompareFunction = .less
        
        depthState = device.makeDepthStencilState(descriptor: depthDescriptor)
        
        sharedUniformBuffer = device.makeBuffer(length: MemoryLayout<Uniforms>.size, options: .storageModeShared)
    }
    
    private func buildDepthTexture() {
        guard let device = layer.device else { return }
        
        let size = self.layer.drawableSize
        
        let descriptor = MTLTextureDescriptor.texture2DDescriptor(
            pixelFormat: .depth32Float,
            width: Int(size.width),
            height: Int(size.height),
            mipmapped: false
        )
        
        descriptor.usage = .renderTarget
        
        //Added for MacOS app
        descriptor.storageMode = .private
        
        depthTexture = device.makeTexture(descriptor: descriptor)
    }
    
    private func buildBoxes() {
        guard let device = layer.device else { return }
        
        if mesh == nil {
            mesh = BoxMesh(device)
        }
        
        boxes.removeAll()
        
        let space = Int32(1.0) * grid.scale
        let cubeSize = Int32(2.0) * grid.scale
        
        let gridSizeX = cubeSize * grid.count.x + space * (grid.count.x - 1)
        let gridSizeY = cubeSize * grid.count.y + space * (grid.count.y - 1)
        let gridSizeZ = cubeSize * grid.count.z + space * (grid.count.z - 1)
        
        var z = -Float(gridSizeZ) / 2.0
        for _ in 0..<grid.count.z {
            var y = -Float(gridSizeY) / 2.0
            for _ in 0..<grid.count.y {
                var x = -Float(gridSizeX) / 2.0
                for _ in 0..<grid.count.x {
                    boxes.append(Box(position: vector_float3(x, y, z), size: vector_float3(1.0, 1.0, 1.0)))
                    x += Float(cubeSize + space)
                }
                y += Float(cubeSize + space)
            }
            z += Float(cubeSize + space)
        }
        
        let length = MemoryLayout<PerInstanceUniforms>.size * boxes.count
        boxUniformBuffer = device.makeBuffer(length: length, options: .storageModeShared)
        
        let length1 = MemoryLayout<BoxSizeUniforms>.size * boxes.count
        boxSizeBuffer = device.makeBuffer(length: length1, options: .storageModeShared)
    }
    
    private func updateUniforms() {
        updateBoxes()
        updateSharedUniforms()
    }
    
    private func updateBoxes() {
        if needsUpdate {
            needsUpdate = false
            buildBoxes()
        }
        
        if rotationVector.x != 0 {
            xAngle += (rotationVector.x > 0 ? 3 : -3) * Float.pi / 180
        }
        
        if rotationVector.y != 0 {
            yAngle += (rotationVector.y > 0 ? 3 : -3) * Float.pi / 180
        }
        
        var rotationMatrix = matrix_identity()
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_rotation(axis: vector_float3(0, 1, 0), angle: xAngle))
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_rotation(axis: vector_float3(1, 0, 0), angle: yAngle))
        
        let scale = 4.0 / Float(max(grid.count.x, grid.count.y, grid.count.z))
        rotationMatrix = matrix_multiply(rotationMatrix, matrix_uniform_scale(scale))
        
        var offset = 0
        boxes.forEach { box in
            let t = matrix_multiply(matrix_translation(box.position), matrix_uniform_scale(Float(grid.scale)))
            let modelMatrix = matrix_multiply(rotationMatrix, t)
            
            let capacity = MemoryLayout<PerInstanceUniforms>.size
            if let p = boxUniformBuffer?.contents().advanced(by: offset).bindMemory(to: PerInstanceUniforms.self, capacity: capacity) {
                p.pointee.modelMatrix = modelMatrix
                p.pointee.normalMatrix = matrix_upper_left3x3(modelMatrix)
                p.pointee.size = vector_float4(box.size.x, box.size.y, box.size.z, 1.0)
            }
            
            offset += MemoryLayout<PerInstanceUniforms>.stride
        }
        
    }
    
    private func updateSharedUniforms() {
        let scale = 1 / Float(grid.scale)
        let viewMatrix = matrix_multiply(matrix_translation(-cameraPosition), matrix_uniform_scale(scale))
        
        let aspect = Float(layer.drawableSize.width / layer.drawableSize.height)
        let fov = (aspect > 1) ? (Float.pi / 4) : (Float.pi / 3)
        let projectionMatrix = matrix_perspective_projection(aspect: aspect, fovy: fov, near: 0.1, far: 100)
        
        if let p = sharedUniformBuffer?.contents().bindMemory(to: Uniforms.self, capacity: MemoryLayout<Uniforms>.size) {
            p.pointee.viewProjectionMatrix = matrix_multiply(projectionMatrix, viewMatrix)
        }
    }
    
    private func createRenderPassWithColorAttachmentTexture(_ texture: MTLTexture) -> MTLRenderPassDescriptor {
        let renderPass = MTLRenderPassDescriptor()
        renderPass.colorAttachments[0].texture = texture
        renderPass.colorAttachments[0].loadAction = .clear
        renderPass.colorAttachments[0].storeAction = .store
        renderPass.colorAttachments[0].clearColor = MTLClearColorMake(1.0, 1.0, 1.0, 1.0)
        
        renderPass.depthAttachment.texture = depthTexture
        renderPass.depthAttachment.loadAction = .clear
        renderPass.depthAttachment.storeAction = .store
        renderPass.depthAttachment.clearDepth = 1.0
        
        return renderPass
    }
    
    func drawBoxesWithCommandEncoder(_ commandEncoder: MTLRenderCommandEncoder) {
        guard let vertexBuffer = mesh?.vertexBuffer,
            let indexBuffer = mesh?.indexBuffer,
            let indexCount = mesh?.indexCount else { return }
        
        commandEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        commandEncoder.setVertexBuffer(sharedUniformBuffer, offset: 0, index: 1)
        commandEncoder.setVertexBuffer(boxUniformBuffer, offset: 0, index: 2)
        commandEncoder.setVertexBuffer(boxSizeBuffer, offset: 0, index: 3)
        
        commandEncoder.drawIndexedPrimitives(
            type: .triangle,
            indexCount: indexCount,
            indexType: .uint16,
            indexBuffer: indexBuffer,
            indexBufferOffset: 0,
            instanceCount: boxes.count
        )
    }
}
