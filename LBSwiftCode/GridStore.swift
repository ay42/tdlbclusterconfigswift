//
//  GridStore.swift
//  TD Cluster Config
//
//  Created by  Nile ÓBroin on 14/04/2019.
//  Copyright © 2019  Nile ÓBroin. All rights reserved.
//

import Foundation

public class GridStore: ObservableObject {

    public init(){}

    private var containerUrl: URL? {
        return FileManager.default.url(forUbiquityContainerIdentifier: nil)?
            .appendingPathComponent("Documents")
    }
    
    public func load<Grid: Decodable>(_ url: URL) throws -> Grid {
        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        return try decoder.decode(Grid.self, from: data)
    }
    
    public func save<Grid: Encodable>(_ grid: Grid) throws {
        guard let url = containerUrl else { return }

        if !FileManager.default.fileExists(atPath: url.path, isDirectory: nil) {
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        }
        
        let fileUrl = url
            .appendingPathComponent("grid_\(Int(Date().timeIntervalSince1970))")
            .appendingPathExtension("json")
        let encoder = JSONEncoder()
        let data = try encoder.encode(grid)
        try data.write(to: fileUrl)
    }
}
