//
//  Grid_Dims.swift
//  TD_LB_Swift
//
//  Created by Nile Ó Broin on 12/04/2019.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//

import Foundation


//let Q_ALGO = 6
let Q_ALGO:Int = 19
//let Q_ALGO = 27



struct Face {

    let starti_src, endi_src:tNi
    let startj_src, endj_src:tNi
    let startk_src, endk_src:tNi


    let starti_dst, endi_dst:tNi
    let startj_dst, endj_dst:tNi
    let startk_dst, endk_dst:tNi


    let l_fill: [Int]
    let l_empty: [Int]

}


struct Edge {

    let starti_src, endi_src:tNi
    let startj_src, endj_src:tNi
    let startk_src, endk_src:tNi


    let starti_dst, endi_dst:tNi
    let startj_dst, endj_dst:tNi
    let startk_dst, endk_dst:tNi


    let l_fill: [Int]
    let l_empty: [Int]

}


struct Corner {

    let starti_src, endi_src:tNi
    let startj_src, endj_src:tNi
    let startk_src, endk_src:tNi


    let starti_dst, endi_dst:tNi
    let startj_dst, endj_dst:tNi
    let startk_dst, endk_dst:tNi


    let l_fill: [Int]
    let l_empty: [Int]

}






struct HaloDims {

    let CellUnitSize, barN, sliceN:tNi
    let sizeN:tNi = -1

    let ForceUnitSize, barF, sliceF:tNi
    let sizeF:tNi = -1


    //ForceReset
    let ForceResetUnitSize:tNi
    let sliceForceReset, barForceReset:tNi
    let sizeForceReset:tNi = -1


    //Nnue
    let NnueUnitSize:tNi
    let sliceNnue, barNnue:tNi
    let sizeNnue:tNi = -1



    let z:tNi = -1
    let y:tNi = -1
    let x:tNi = -1
    let xg,  yg, zg:tNi
    let xg0, yg0, zg0:tNi
    let xg1, yg1, zg1:tNi


    var face: [Face]
    var edge: [Edge]
    var corner: [Corner]

}







