//
//  Grid_Dims.swift
//  TD_LB_Swift
//
//  Created by Nile Ó Broin on 12/04/2019.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//



typealias t3d = Int
typealias tNi = Int32

typealias tQvec = Float32

typealias tGeomShape = Float32

typealias tStep = Int16





