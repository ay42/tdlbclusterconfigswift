//
//  SecondaryGrid.swift
//  LBSwiftFrameworkiOS
//
//  Created by MacMaster on 8/6/19.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//

import Foundation

public struct SecondaryGrid {
    public var depth: vector_float3
    public var nDevice: Int32
    public var scale : Int32 = 1
    
    public enum CodingKeys: String, CodingKey {
        case depthX
        case depthY
        case depthZ
        case nDevice
    }
    
    public init(depth: vector_float3, spacing: Int32){
        self.depth = depth;
        self.nDevice = spacing
    }
}

extension SecondaryGrid: Decodable {
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let x = Float(try values.decode(Int.self, forKey: .depthX))
        let y = Float(try values.decode(Int.self, forKey: .depthY))
        let z = Float(try values.decode(Int.self, forKey: .depthZ))
        depth = vector_float3(x, y, z)
        
        nDevice = Int32(try values.decode(Int.self, forKey: .nDevice))
    }
}

extension SecondaryGrid: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(depth.x, forKey: .depthX)
        try container.encode(depth.y, forKey: .depthY)
        try container.encode(depth.z, forKey: .depthZ)
        
        try container.encode(nDevice, forKey: .nDevice)
    }
}
