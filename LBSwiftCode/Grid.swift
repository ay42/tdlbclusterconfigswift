//
//  Grid.swift
//  TD Cluster Config
//
//  Created by  Nile ÓBroin on 14/04/2019.
//  Copyright © 2019  Nile ÓBroin. All rights reserved.
//

import Foundation

public struct Grid {
    public var count: vector_int3
    public var scale: Int32

    public enum CodingKeys: String, CodingKey {
        case ngx
        case ngy
        case ngz
        case scale
    }

    public init(count: vector_int3, scale: Int32){
        self.count = count;
        self.scale = scale;
    }
}

extension Grid: Decodable {
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let x = Int32(try values.decode(Int.self, forKey: .ngx))
        let y = Int32(try values.decode(Int.self, forKey: .ngy))
        let z = Int32(try values.decode(Int.self, forKey: .ngz))
        count = vector_int3(x, y, z)
        
        scale = Int32(try values.decode(Int.self, forKey: .scale))
    }
}

extension Grid: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(count.x, forKey: .ngx)
        try container.encode(count.y, forKey: .ngy)
        try container.encode(count.z, forKey: .ngz)
        
        try container.encode(scale, forKey: .scale)
    }
}
