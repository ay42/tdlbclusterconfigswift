//
//  Grid_Dims.swift
//  TD_LB_Swift
//
//  Created by Nile Ó Broin on 12/04/2019.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//

import Foundation


struct NodeDims_Fix {


    let idi:t3d
    let idj:t3d
    let idk:t3d


    let node000:Int
    let ncpu:Int
    let rank:Int

    let nthreads:Int
    let numprocs:Int


    let nDevice:tNi

    let depth_halo_x:tNi
    let depth_halo_y:tNi
    let depth_halo_z:tNi


    let x:tNi
    let y:tNi
    let z:tNi


    let xg:tNi
    let yg:tNi
    let zg:tNi


    let xg0:tNi
    let yg0:tNi
    let zg0:tNi


    let xg1:tNi
    let yg1:tNi
    let zg1:tNi


}







