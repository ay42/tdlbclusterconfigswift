//
//  LBSwiftFrameworkMacOS.h
//  LBSwiftFrameworkMacOS
//
//  Created by Nile Ó Broin on 18/04/2019.
//  Copyright © 2019 Turbulent Dynamics. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for LBSwiftFrameworkMacOS.
FOUNDATION_EXPORT double LBSwiftFrameworkMacOSVersionNumber;

//! Project version string for LBSwiftFrameworkMacOS.
FOUNDATION_EXPORT const unsigned char LBSwiftFrameworkMacOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LBSwiftFrameworkMacOS/PublicHeader.h>


#import <LBSwiftFrameworkMacOS/Common.h>
