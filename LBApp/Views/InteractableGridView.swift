import SwiftUI
import MetalKit
#if os(iOS)
import LBSwiftFrameworkiOS
#else
import LBSwiftFrameworkMacOS
#endif

struct InteractableGridView<ViewRenderer: Renderer>: View {
    @Binding var gridView: GridView<ViewRenderer>

    var body: some View {
        AnyView(gridView)
            .gesture(
                DragGesture()
                    .onChanged { value in
                        let velocityX = value.predictedEndLocation.x - value.location.x
                        let velocityY = value.predictedEndLocation.y - value.location.y
                        let p = -(vector_float2(Float(velocityX), Float(velocityY)))
                        self.gridView.renderer.rotationVector = clamp(p, min: -1.0, max: 1.0)
                    }
                    .onEnded { _ in
                        self.gridView.renderer.rotationVector = vector_float2(0, 0)
                    }
            )
    }
}

struct InteractableGridView_Previews: PreviewProvider {
    static var previews: some View {
        InteractableGridView(gridView: .constant(GridView<FirstRenderer>()))
    }
}
