import SwiftUI

struct ActionControlView: View {
    var body: some View {
        HStack(alignment: .center, spacing: 50) {
            Button("Load") {
                
            }
            Button("Update") {
                
            }
            Button("Save") {
                
            }
        }
    }
}

struct ActionControlView_Previews: PreviewProvider {
    static var previews: some View {
        ActionControlView()
    }
}
