import Foundation
import SwiftUI
#if os(iOS)
import LBSwiftFrameworkiOS
#else
import LBSwiftFrameworkMacOS
#endif

protocol Control {
    associatedtype ControlledRenderer: Renderer
    init(renderer: ControlledRenderer)
}
