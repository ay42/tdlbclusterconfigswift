import SwiftUI
import MetalKit
#if os(iOS)
import LBSwiftFrameworkiOS
#else
import LBSwiftFrameworkMacOS
#endif

struct FirstControlView: View, Control {
    typealias ControlledRenderer = FirstRenderer
    
    var renderer: FirstRenderer
    
    var body: some View {
        VStack {
            HStack {
                Text("ngx:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridCountXFloatBinding, in: 1...6, step: 1)
            }
            HStack {
                Text("ngy:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridCountYFloatBinding, in: 1...6, step: 1)
            }
            HStack {
                Text("ngz:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridCountZFloatBinding, in: 1...6, step: 1)
            }
            HStack {
                Text("zoom:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridScaleFloatBinding, in: 1...6, step: 1)
            }
            ActionControlView()
        }
        .padding()
        .background(Color.secondary.opacity(0.2))
    }
}

extension FirstRenderer {
    
    var gridCountXFloatBinding: Binding<Float> {
        Binding<Float>( get: { Float(self.grid.count.x) }, set: { self.grid.count.x = Int32($0) })
    }
    
    var gridCountYFloatBinding: Binding<Float> {
        Binding<Float>(get: { Float(self.grid.count.y) }, set: { value, _ in self.grid.count.y = Int32(value) })
    }
    
    var gridCountZFloatBinding: Binding<Float> {
        Binding<Float>(get: { Float(self.grid.count.z) }, set: { value, _ in self.grid.count.z = Int32(value) })
    }
    
    var gridScaleFloatBinding: Binding<Float> {
        Binding(get: { Float(self.grid.scale) }, set: { value, _ in self.grid.scale = Int32(value) })
    }
}
