import SwiftUI
import MetalKit
#if os(iOS)
import LBSwiftFrameworkiOS
#else
import LBSwiftFrameworkMacOS
#endif

struct PageView<ViewRenderer, ControlView: Control & View>: View where ViewRenderer == ControlView.ControlledRenderer, ViewRenderer.RenderedGrid : Codable  {
    @EnvironmentObject var gridStore: GridStore
    @State var gridView = GridView<ViewRenderer>()
    
    var body: some View {
        VStack(alignment: .center) {
            InteractableGridView(gridView: $gridView)
            ControlView(renderer: gridView.renderer)
        }
    }
    
//    private func loadGrid() {
//        let picker = UIDocumentPickerViewController(documentTypes: ["public.json"], in: .import)
//        picker.delegate = self
//        picker.modalPresentationStyle = .fullScreen
//        picker.allowsMultipleSelection = false
//        present(picker, animated: true, completion: nil)
//    }
//    
//    private func saveGrid() {
//        guard let grid = gridView.renderer?.getGrid() else { return }
//        
//        DispatchQueue.global().async {
//            do {
//                try self.gridStore.save(grid)
//            } catch {
//                print("PageViewController: can't save grid: \(error)")
//            }
//        }
//    }
//    
//    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        guard let url = urls.first else { return }
//        
//        do {
//            let grid: ViewRenderer.RenderedGrid = try gridStore.load(url)
//            gridView.renderer?.setGrid(grid)
//            
//        } catch {
//            print("FirstPageViewController: can't load grid: \(error)")
//        }
//    }
}

struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView<FirstRenderer, FirstControlView>()
    }
}
