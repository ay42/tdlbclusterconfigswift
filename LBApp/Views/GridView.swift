import MetalKit
import SwiftUI

#if os(iOS)
import UIKit
import LBSwiftFrameworkiOS
typealias PlatformView = UIView
typealias DisplayLink = CADisplayLink
#else
import AppKit
import LBSwiftFrameworkMacOS
typealias PlatformView = NSView
typealias DisplayLink = CVDisplayLink
#endif

final class GridView<ViewRenderer: Renderer>: PlatformView {
    
    let metalLayer = CAMetalLayer()
    var displayLink: DisplayLink?
    var renderer: ViewRenderer
    
    #if os(iOS)
    
    override init(frame: CGRect) {
        renderer = ViewRenderer(layer: metalLayer)
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        layer.addSublayer(metalLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        metalLayer.frame = bounds
    }
        
    @objc private func update() {
        renderer.draw()
    }
    
    /// Fix this
    deinit {
        displayLink?.remove(from: RunLoop.main, forMode: .common)
    }
    
    #else

    override init(frame frameRect: NSRect) {
        renderer = ViewRenderer(layer: metalLayer)
        /// Passing a zero frame here will result in CAMetalLayer drawable to be nil
        /// Overriding with non zero frame
        let frame = CGRect(x: 0, y: 0, width: 1000, height: 1000)
        super.init(frame: frame)
        wantsLayer = true
        metalLayer.allowsNextDrawableTimeout = false
        layer = metalLayer
        
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError()
    }
    
    func setupDisplayLink() {
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink)

        if let link = displayLink {
            CVDisplayLinkSetOutputHandler(link) { [weak self] _, _, _, _, _ in
                self?.renderer.draw()
                return kCVReturnSuccess
            }

            CVDisplayLinkStart(link)
        }
    }
    #endif
}

#if os(iOS)
extension GridView: UIViewRepresentable {
    func makeUIView(context: UIViewRepresentableContext<GridView<ViewRenderer>>) -> GridView<ViewRenderer> {
        self.displayLink = CADisplayLink(target: self, selector: #selector(update))
        displayLink?.add(to: RunLoop.main, forMode: .common)
        return self
    }
    
    func updateUIView(_ uiView: GridView<ViewRenderer>, context: UIViewRepresentableContext<GridView<ViewRenderer>>) {
    }
}

#else

extension GridView: NSViewRepresentable {
    func makeNSView(context: NSViewRepresentableContext<GridView<ViewRenderer>>) -> GridView<ViewRenderer> {
        setupDisplayLink()
        return self
    }
    
    func updateNSView(_ nsView: GridView<ViewRenderer>, context: NSViewRepresentableContext<GridView<ViewRenderer>>) {
    }
}

#endif
