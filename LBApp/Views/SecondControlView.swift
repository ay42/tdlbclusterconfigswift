import SwiftUI
import MetalKit
#if os(iOS)
import LBSwiftFrameworkiOS
#else
import LBSwiftFrameworkMacOS
#endif

struct SecondControlView: View, Control {
    typealias ControlledRenderer = SecondRenderer
    
    var renderer: SecondRenderer
    
    var body: some View {
        VStack {
            HStack {
                Text("depthX:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridDepthXFloatBinding, in: 1...6)
            }
            HStack {
                Text("depthY:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridDepthYFloatBinding, in: 1...6)
            }
            HStack {
                Text("depthZ:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridDepthZFloatBinding, in: 1...6)
            }
            HStack {
                Text("nDevice:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridNDeviceFloatBinding, in: 1...64, step: 1)
            }
            HStack {
                Text("zoom:")
                    .frame(width: 100)
                Slider(value: self.renderer.gridScaleFloatBinding, in: 1...6, step: 1)
            }
            ActionControlView()
        }
        .padding()
        .background(Color.secondary.opacity(0.2))
    }
}

extension SecondRenderer {
    
    var gridDepthXFloatBinding: Binding<Float> {
        Binding<Float>( get: { Float(self.grid.depth.x) }, set: { self.grid.depth.x = $0 })
    }
    
    var gridDepthYFloatBinding: Binding<Float> {
        Binding<Float>( get: { Float(self.grid.depth.y) }, set: { self.grid.depth.y = $0 })
    }
    
    var gridDepthZFloatBinding: Binding<Float> {
        Binding<Float>( get: { Float(self.grid.depth.z) }, set: { self.grid.depth.z = $0 })
    }
    
    var gridNDeviceFloatBinding: Binding<Float> {
        Binding<Float>( get: { Float(self.grid.nDevice) }, set: { self.grid.nDevice = Int32($0) })
    }
    
    var gridScaleFloatBinding: Binding<Float> {
        Binding(get: { Float(self.grid.scale) }, set: { value, _ in self.grid.scale = Int32(value) })
    }
}
